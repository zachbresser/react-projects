// Arrow Functions
var foo = function(a, b) {
  return a + b;
};

// arrow function
var foo = (a, b) => {
  return a + b;
}


do.something((a, b) => { return a + b; });
do.something((a, b) => a + b); // implicit return same as above

[0, 1, 2].map(val => val++); // [1, 2, 3]

// Lexical context binding
var module = {
  age: 30,
  foo: function() {
    console.log(this.age);
  }
};

module.foo(); //logs 30

// without context
var module = {
  age: 30,
  foo: function() {
    setTimeout(function() {
      console.log(this.age); // doesn't work, out of context
    }, 100);
  }
};

module.foo();

// with context
var module = {
  age: 30,
  foo: function() {
    setTimeout(() => {
      console.log(this.age); // doesn't work, out of context
    }, 100);
  }
};

module.foo();

// same as
// without context
var module = {
  age: 30,
  foo: function() {
    setTimeout(function() {
      console.log(this.age);
    }.bind(this), 100); // notice bind
  }
};

module.foo();
