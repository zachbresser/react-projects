// template strings


// previously
var name = "will";
var thing = "party";
var gree = "hi, my naem is " + name + 'and i like to ' + thing;

// now
var greet = `hi, my name is ${name} and I like to ${thing}`;

// multi-line
var greet = `hi, my name is ${name}
 and I like to
 ${thing}`; // also valid
