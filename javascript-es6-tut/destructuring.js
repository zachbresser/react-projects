// destructuring
var foo = {
  bar: 1,
  baz: 2
}

var { bar, baz } = foo;

// ex2

var tenses = ['me', 'you', 'he'];
var [ firstPerson, secondPerson ] = tenses;

// ex 3

var foo = 2;

/* same as:
   var obj = {
     bar: 1,
     foo: 2
   }
*/
var obj = {
  bar: 1,
  foo
}

// ex4
function calcBmi({ weight: w, height: h, max = 25, callback }) {
  var bmi = w / Math.pow(h, 2);
  if (bmi > max) {
    console.log("You're overweight");
  }
  if (callback) {
    callback(bmi);
  }

}
calcBmi({ weight, height, 25 });
calcBmi({ weight, height, function() {} });
