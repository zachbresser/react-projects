// scoping

var a = 1;

function() {
  var b = 2;
}

console.log(b); // b is out of scope at this point. its only valid in func

var a = 1;
if(true) {
  var b = 2;
}

console.log(b); // b is 2

// block scoping
var a = 1;
for (20) {
  let b = 2; // only accessible in for loop, and is recreated on every loop.
}

console.log(b); // not accessible outside of loop
