import React from "react";
import "../assets/Sidebar.css";
import User from "../containers/User";

class Sidebar extends React.Component {
  render() {
    const { contacts } = this.props;
    return (
      <aside className="Sidebar">
        {contacts.map(contact => <User user={contact} key={contact.user_id} />)}
      </aside>
    );
  }
}

export default Sidebar;
