import React from "react";
import "../assets/Main.css";
import Empty from "../components/Empty";
import ChatWindow from "../containers/ChatWindow";

class Main extends React.Component {
  render() {
    const { user, activeUserId } = this.props;
    return (
      <main className="Main">
        {!activeUserId ? (
          <Empty user={user} activeUserId={activeUserId} />
        ) : (
          <ChatWindow activeUserId={activeUserId} />
        )}
      </main>
    );
  }
}

export default Main;
