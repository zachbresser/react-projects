import React from "react";
import store from "../store";
import Header from "../components/Header";
import MessageInput from "./MessageInput";
import Chats from "../components/Chats";
import "../assets/ChatWindow.css";
import _ from "lodash";

class ChatWindow extends React.Component {
  render() {
    const { activeUserId } = this.props;
    const curState = store.getState();
    const activeUser = curState.contacts[activeUserId];
    const activeMsgs = curState.messages[activeUserId];
    const { typing } = curState;

    return (
      <div className="ChatWindow">
        <Header user={activeUser} />
        <Chats messages={_.values(activeMsgs)} />
        <MessageInput value={typing} />
      </div>
    );
  }
}

export default ChatWindow;
