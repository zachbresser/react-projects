import React from "react";
import store from "../store";
import { setTypingValue, sendMessage } from "../actions";
import "../assets/MessageInput.css";

class MessageInput extends React.Component {
  constructor(props) {
    super(props);
  }
  handleChange = e => {
    store.dispatch(setTypingValue(e.target.value));
  };

  handleSubmit = e => {
    this.curState = store.getState();
    e.preventDefault();
    store.dispatch(
      sendMessage(this.curState.typing, this.curState.activeUserId)
    );
  };

  render() {
    const { value } = this.props;
    return (
      <form className="Message" onSubmit={this.handleSubmit}>
        <input
          className="Message__input"
          onChange={this.handleChange}
          value={value}
          placeholder="Write a message"
        />
      </form>
    );
  }
}

export default MessageInput;
