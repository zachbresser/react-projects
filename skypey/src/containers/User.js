import React from "react";
import "../assets/User.css";
import store from "../store";
import { setActiveUserId } from "../actions";

class User extends React.Component {
  constructor(props) {
    super(props);

    this.handleUserClick = this.handleUserClick.bind(this, this.props.user);
  }

  handleUserClick({ user_id }) {
    store.dispatch(setActiveUserId(user_id));
  }

  render() {
    const { name, profile_pic, status } = this.props.user;

    return (
      <div className="User" onClick={this.handleUserClick}>
        <img src={profile_pic} alt={name} className="User__pic" />
        <div className="User__details">
          <p className="User__details-name">{name}</p>
          <p className="User__details-status">{status}</p>
        </div>
      </div>
    );
  }
}

export default User;
