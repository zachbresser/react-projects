import { getMessages } from "../assets/staticData";
import { SEND_MESSAGE } from "../constants/action-types";
import _ from "lodash";

export default function messages(state = getMessages(10), action) {
  switch (action.type) {
    case SEND_MESSAGE:
      const { message, activeUserId } = action.payload;
      console.log("Message: ", message);
      console.log("UserID: ", activeUserId);
      const allUserMsgs = state[activeUserId];
      const number = +_.keys(allUserMsgs).pop() + 1;
      return {
        ...state,
        [activeUserId]: {
          ...allUserMsgs,
          [number]: {
            number,
            text: message,
            is_user_msg: true
          }
        }
      };
    default:
      return state;
  }
}
