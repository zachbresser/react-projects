import React, { Component } from 'react';
import './App.css';
import sunny from './sunny.svg';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="container">
          <div className="row">
            <div className="col-md-2">
              <WeatherApp />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

class WeatherDay extends Component {
  render() {
    const { name, tempHigh, tempLow, weather } = this.props.day;

    return (
      <div className="container day-container">
        <div className="row">
          <div className="col text-center">
            {name}
          </div>
        </div>
        <div className="row">
          <div className="col">
            <object data={weather} type="image/svg+xml" className="svg-symbol-sun">
              <img src={weather} alt="Weather" className="mx-auto" />
            </object>
          </div>
        </div>
        <div className="row">
          <div className="col text-center">
            <strong>{tempHigh}&#176;</strong>
          </div>
          <div className="col text-center">
            {tempLow}&#176;
          </div>
        </div>
      </div>
    );
  }
}
class WeatherApp extends Component {
  render() {
    const days = {
      name: "Mon",
      tempHigh: "71",
      tempLow: "61",
      weather: sunny
    };
    return (
        <WeatherDay
          day={days}
        />
    );
  }
}
export default App;
