import React, { Component } from 'react';

import profilePic from './profilePic.png';
import learningReact from './learning-react.jpg';

import './App.css';

class App extends Component {
  render() {

    const posts = [
      {
        comments: 2,
        retweets: 43,
        favorites: 190,
        name: "The Practical Dev",
        username: "ThePraticalDev",
        postDate: "Sep 10",
        postDescription: "Learning React? Start Small.",
        image: learningReact,
        cardDescription: "Learning React? Start Small. \
          Can't pry yourself away from the tutorials? The cure is to make tiny little experiment apps",
        descriptionLink: "dev.to"
      },
      {
        comments: 12,
        retweets: 73,
        favorites: 356,
        name: "The Practical Dev",
        username: "ThePraticalDev",
        postDate: "Sep 09",
        postDescription: "I'm starting to learn React!",
        image: learningReact,
        cardDescription: "This is a test \
          of my React App knowledge. I hope I can continue to learn",
        descriptionLink: "dev.to"
      },
    ];

    return (
      <div className="App">
        {
          posts.map(post => {
            return (
              <SocialCard
                post={post}
              />
            );
          })        
        }
      </div>
    );
  }
}

class SocialBar extends Component {
  render() {
    const { comments, retweets, favorites } = this.props;
    return (
      <div id="social-bar">
        <ul>
          <li>
            <i className="far fa-comment"></i><span className='social-bar-value'>&nbsp;{comments}</span>
          </li>
          <li>
            <i className="fas fa-retweet retweeted"></i><span className="social-bar-value retweeted">&nbsp;{retweets}</span>
          </li>
          <li>
            <i className="fas fa-heart favorited"></i><span className="social-bar-value favorited">&nbsp;{favorites}</span>
          </li>
          <li>
            <i className="far fa-envelope"></i>
          </li>
        </ul>
      </div>
    );
  }
}

class SocialCardImage extends Component {
  render() {
    const { image, cardDescription, descriptionLink } = this.props;

    return (
      <div className="card-image-container">
        <img src={image} className="card-image" alt="Post"/>
        <div className="card-image-description">
          <p>{cardDescription}<br /></p>
          <i className="description-link">{descriptionLink}</i>
        </div>
      </div>
    );
  }
}
class SocialCardHeading extends Component {
  render() {
    const { name, username, postDate, postDescription } = this.props;

    return (
      <div id="social-heading-container">
        <div className="social-heading">
          <div className="social-user">
            <strong>{name} &nbsp;</strong>
            <i>@{username} &nbsp;</i>
            &middot;&nbsp;
            <span className="user-post-date">{postDate}</span>
            <div className="user-options-arrow"><i className="fas fa-angle-down"></i></div>
          </div>
          <div className="social-post-description">
            <p>{postDescription}</p>
          </div>
        </div>
      </div>
    );
  }
}
class SocialCard extends Component {
  render() {
    const {
      comments, retweets, favorites, // SocialBar
      name, username, postDate, postDescription, // SocialCardHeading
      image, cardDescription, descriptionLink // SocialCardImage
    } = this.props.post;

    return (
      <div className="social-card">
        <div className="image-container">
          <img src={profilePic} className="social-profile-pic" alt="profile" />
        </div>
        <SocialCardHeading
          name={name}
          username={username}
          postDate={postDate}
          postDescription={postDescription}
        />
        <SocialCardImage
          image={image}
          cardDescription={cardDescription}
          descriptionLink={descriptionLink}
        />
        <SocialBar
          comments={comments}
          retweets={retweets}
          favorites={favorites}
        />
      </div>
    );
  }
}
export default App;
