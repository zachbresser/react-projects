import React, { Component } from 'react';

import Comments from './Comments';

/**
 * Main Web App
 * @extends Component
 */
class App extends Component {
  render() {
    return (
      <Comments />
    );
  }
}

export default App;
