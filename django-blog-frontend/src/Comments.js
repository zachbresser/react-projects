import React, { Component } from 'react';

/**
 * Comments Component
 * @extends Component
 */
class Comments extends Component {
  /**
   * Constructor for Comments
   */
  constructor(props) {
    super(props);
    this.state = {
      comments: []
    };
  }

  /**
   * After mounting component, make AJAX request to API and get comments
   * @return {Promise} Not Used, sets comments var in this.state
   */
  async componentDidMount() {
    try {
      // fetch comments from API
      const res = await fetch('http://10.105.7.78/api/v1/comments/');
      // await response
      const comments = await res.json();

      // loop over contents and get author username
      for(var i = 0; i < comments.length; i++) {
        const resp = await fetch('http://10.105.7.78/api/v1/users/' + comments[i].author + '/');
        const author = await resp.json();
        console.log(author);
        comments[i].author = author.username;
      }
      // set this.state.comments to res.json();
      this.setState({
        comments
      });
      // if error
    } catch (e) {
      // log error
      console.log(e);
    }
  }

  /**
   * Render the HTML
   * @return {HTML} List of comments
   */
  render() {
    return (
      <div key={this.state.comments}>
        {this.state.comments.map((item, index) => (
          <div key={index - 1}>
           <h4 key={item.author}>{item.author}</h4>
           <h6 key={index}>{item.content}</h6>
          </div>
        ))}
      </div>
    );
  }
}

export default Comments;
